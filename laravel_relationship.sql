-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 10, 2019 at 04:58 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_relationship`
--

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `state_id` bigint(20) NOT NULL,
  `city_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `state_id`, `city_name`, `created_at`, `updated_at`) VALUES
(1, 1, 'Us city', NULL, NULL),
(2, 2, 'Coimbatore', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `country_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `country_name`, `created_at`, `updated_at`) VALUES
(1, 'IN', '2019-05-09 12:24:30', '2019-05-09 12:24:30'),
(2, 'Nolan Herzog Jr.', '2019-05-09 12:24:30', '2019-05-09 12:24:30'),
(3, 'Mariane Feeney', '2019-05-09 12:24:30', '2019-05-09 12:24:30'),
(4, 'USA', '2019-05-09 12:24:30', '2019-05-09 12:24:30'),
(5, 'Donald Maggio V', '2019-05-09 12:24:30', '2019-05-09 12:24:30');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_05_09_023050_create_passport_table', 1),
(4, '2019_05_09_145054_create_mobiles_table', 1),
(5, '2019_05_09_164021_create_role_table', 1),
(6, '2019_05_09_164126_create_user_role_table', 1),
(7, '2019_05_09_173451_create_country_table', 2),
(8, '2019_05_09_173522_create_state_table', 2),
(9, '2019_05_09_173547_create_city_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `mobiles`
--

CREATE TABLE `mobiles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mobiles`
--

INSERT INTO `mobiles` (`id`, `user_id`, `mobile`, `created_at`, `updated_at`) VALUES
(1, 1, '62367404', NULL, NULL),
(2, 1, '48481023', NULL, NULL),
(3, 2, '66796047', NULL, NULL),
(4, 2, '96034499', NULL, NULL),
(5, 3, '27432951', NULL, NULL),
(6, 3, '60054476', NULL, NULL),
(7, 4, '30213011', NULL, NULL),
(8, 4, '83207397', NULL, NULL),
(9, 5, '36467196', NULL, NULL),
(10, 5, '99549804', NULL, NULL),
(11, 6, '48828164', NULL, NULL),
(12, 6, '73627630', NULL, NULL),
(13, 7, '73727472', NULL, NULL),
(14, 7, '83401034', NULL, NULL),
(15, 8, '41740616', NULL, NULL),
(16, 8, '68080071', NULL, NULL),
(17, 9, '54764604', NULL, NULL),
(18, 9, '45039412', NULL, NULL),
(19, 10, '57971273', NULL, NULL),
(20, 10, '40186563', NULL, NULL),
(21, 11, '15853084', NULL, NULL),
(22, 11, '36171905', NULL, NULL),
(23, 12, '45660085', NULL, NULL),
(24, 12, '77850711', NULL, NULL),
(25, 13, '18612051', NULL, NULL),
(26, 13, '28646583', NULL, NULL),
(27, 14, '85359086', NULL, NULL),
(28, 14, '86387243', NULL, NULL),
(29, 15, '10779553', NULL, NULL),
(30, 15, '68845219', NULL, NULL),
(31, 16, '42581297', NULL, NULL),
(32, 16, '82810261', NULL, NULL),
(33, 17, '86989373', NULL, NULL),
(34, 17, '86955803', NULL, NULL),
(35, 18, '38819446', NULL, NULL),
(36, 18, '45488037', NULL, NULL),
(37, 19, '84651709', NULL, NULL),
(38, 19, '81739799', NULL, NULL),
(39, 20, '35546309', NULL, NULL),
(40, 20, '58829598', NULL, NULL),
(41, 21, '93148532', NULL, NULL),
(42, 21, '19733666', NULL, NULL),
(43, 22, '39895318', NULL, NULL),
(44, 22, '70701857', NULL, NULL),
(45, 23, '24125302', NULL, NULL),
(46, 23, '74107411', NULL, NULL),
(47, 24, '68156450', NULL, NULL),
(48, 24, '27852748', NULL, NULL),
(49, 25, '63300267', NULL, NULL),
(50, 25, '81758151', NULL, NULL),
(51, 26, '17466455', NULL, NULL),
(52, 26, '87856585', NULL, NULL),
(53, 27, '68770553', NULL, NULL),
(54, 27, '58793822', NULL, NULL),
(55, 28, '87620082', NULL, NULL),
(56, 28, '67052436', NULL, NULL),
(57, 29, '38900071', NULL, NULL),
(58, 29, '26472552', NULL, NULL),
(59, 30, '43209385', NULL, NULL),
(60, 30, '65846072', NULL, NULL),
(61, 31, '38949157', NULL, NULL),
(62, 31, '85331164', NULL, NULL),
(63, 32, '58266168', NULL, NULL),
(64, 32, '18977796', NULL, NULL),
(65, 33, '34282025', NULL, NULL),
(66, 33, '88264170', NULL, NULL),
(67, 34, '54014626', NULL, NULL),
(68, 34, '44358658', NULL, NULL),
(69, 35, '23045560', NULL, NULL),
(70, 35, '13767565', NULL, NULL),
(71, 36, '18113250', NULL, NULL),
(72, 36, '61356408', NULL, NULL),
(73, 37, '78263274', NULL, NULL),
(74, 37, '62375505', NULL, NULL),
(75, 38, '30754682', NULL, NULL),
(76, 38, '83152344', NULL, NULL),
(77, 39, '19149969', NULL, NULL),
(78, 39, '31272979', NULL, NULL),
(79, 40, '82787463', NULL, NULL),
(80, 40, '23808092', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `passports`
--

CREATE TABLE `passports` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `passports`
--

INSERT INTO `passports` (`id`, `user_id`, `number`, `created_at`, `updated_at`) VALUES
(1, 1, '621347600', NULL, NULL),
(2, 2, '585497434', NULL, NULL),
(3, 3, '704825026', NULL, NULL),
(4, 4, '711914045', NULL, NULL),
(5, 5, '734899628', NULL, NULL),
(6, 6, '594555464', NULL, NULL),
(7, 7, '770655183', NULL, NULL),
(8, 8, '955225165', NULL, NULL),
(9, 9, '886511590', NULL, NULL),
(10, 10, '878239425', NULL, NULL),
(11, 11, '254048606', NULL, NULL),
(12, 12, '471312740', NULL, NULL),
(13, 13, '857039815', NULL, NULL),
(14, 14, '210152732', NULL, NULL),
(15, 15, '703958262', NULL, NULL),
(16, 16, '247781465', NULL, NULL),
(17, 17, '597252493', NULL, NULL),
(18, 18, '787437723', NULL, NULL),
(19, 19, '708451806', NULL, NULL),
(20, 20, '290697292', NULL, NULL),
(21, 21, '541242475', NULL, NULL),
(22, 22, '910278095', NULL, NULL),
(23, 23, '743853968', NULL, NULL),
(24, 24, '777594283', NULL, NULL),
(25, 25, '536944180', NULL, NULL),
(26, 26, '654757614', NULL, NULL),
(27, 27, '237467999', NULL, NULL),
(28, 28, '995578790', NULL, NULL),
(29, 29, '755750650', NULL, NULL),
(30, 30, '257144042', NULL, NULL),
(31, 31, '702955940', NULL, NULL),
(32, 32, '607982922', NULL, NULL),
(33, 33, '360372274', NULL, NULL),
(34, 34, '585982480', NULL, NULL),
(35, 35, '684669472', NULL, NULL),
(36, 36, '439635004', NULL, NULL),
(37, 37, '241573332', NULL, NULL),
(38, 38, '790905398', NULL, NULL),
(39, 39, '618201796', NULL, NULL),
(40, 40, '542161721', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role_name`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', NULL, NULL),
(2, 'Admin', NULL, NULL),
(3, 'Super Admin', NULL, NULL),
(4, 'Admin', NULL, NULL),
(5, 'Super Admin', NULL, NULL),
(6, 'Admin', NULL, NULL),
(7, 'Super Admin', NULL, NULL),
(8, 'Admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 1, 2, NULL, NULL),
(3, 2, 1, NULL, NULL),
(4, 2, 2, NULL, NULL),
(5, 3, 1, NULL, NULL),
(6, 3, 2, NULL, NULL),
(7, 4, 1, NULL, NULL),
(8, 4, 2, NULL, NULL),
(9, 5, 1, NULL, NULL),
(10, 5, 2, NULL, NULL),
(11, 6, 1, NULL, NULL),
(12, 6, 2, NULL, NULL),
(13, 7, 1, NULL, NULL),
(14, 7, 2, NULL, NULL),
(15, 8, 1, NULL, NULL),
(16, 8, 2, NULL, NULL),
(17, 9, 1, NULL, NULL),
(18, 9, 2, NULL, NULL),
(19, 10, 1, NULL, NULL),
(20, 10, 2, NULL, NULL),
(21, 11, 1, NULL, NULL),
(22, 11, 2, NULL, NULL),
(23, 12, 1, NULL, NULL),
(24, 12, 2, NULL, NULL),
(25, 13, 1, NULL, NULL),
(26, 13, 2, NULL, NULL),
(27, 14, 1, NULL, NULL),
(28, 14, 2, NULL, NULL),
(29, 15, 1, NULL, NULL),
(30, 15, 2, NULL, NULL),
(31, 16, 1, NULL, NULL),
(32, 16, 2, NULL, NULL),
(33, 17, 1, NULL, NULL),
(34, 17, 2, NULL, NULL),
(35, 18, 1, NULL, NULL),
(36, 18, 2, NULL, NULL),
(37, 19, 1, NULL, NULL),
(38, 19, 2, NULL, NULL),
(39, 20, 1, NULL, NULL),
(40, 20, 2, NULL, NULL),
(41, 21, 1, NULL, NULL),
(42, 21, 2, NULL, NULL),
(43, 22, 1, NULL, NULL),
(44, 22, 2, NULL, NULL),
(45, 23, 1, NULL, NULL),
(46, 23, 2, NULL, NULL),
(47, 24, 1, NULL, NULL),
(48, 24, 2, NULL, NULL),
(49, 25, 1, NULL, NULL),
(50, 25, 2, NULL, NULL),
(51, 26, 1, NULL, NULL),
(52, 26, 2, NULL, NULL),
(53, 27, 1, NULL, NULL),
(54, 27, 2, NULL, NULL),
(55, 28, 1, NULL, NULL),
(56, 28, 2, NULL, NULL),
(57, 29, 1, NULL, NULL),
(58, 29, 2, NULL, NULL),
(59, 30, 1, NULL, NULL),
(60, 30, 2, NULL, NULL),
(61, 31, 1, NULL, NULL),
(62, 31, 2, NULL, NULL),
(63, 32, 1, NULL, NULL),
(64, 32, 2, NULL, NULL),
(65, 33, 1, NULL, NULL),
(66, 33, 2, NULL, NULL),
(67, 34, 1, NULL, NULL),
(68, 34, 2, NULL, NULL),
(69, 35, 1, NULL, NULL),
(70, 35, 2, NULL, NULL),
(71, 36, 1, NULL, NULL),
(72, 36, 2, NULL, NULL),
(73, 37, 1, NULL, NULL),
(74, 37, 2, NULL, NULL),
(75, 38, 1, NULL, NULL),
(76, 38, 2, NULL, NULL),
(77, 39, 1, NULL, NULL),
(78, 39, 2, NULL, NULL),
(79, 40, 1, NULL, NULL),
(80, 40, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `country_id` bigint(20) NOT NULL,
  `state_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `country_id`, `state_name`, `created_at`, `updated_at`) VALUES
(1, 1, 'TN', NULL, NULL),
(2, 3, 'Los Angels', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Edwin Klocko', 'ella.schneider@example.net', '2019-05-09 11:43:58', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'QPsAKrOUjv', '2019-05-09 11:43:58', '2019-05-09 11:43:58'),
(2, 'Osvaldo Aufderhar', 'kelsi.huel@example.org', '2019-05-09 11:43:58', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'lv9n5VLQYJ', '2019-05-09 11:43:58', '2019-05-09 11:43:58'),
(3, 'Dr. Josh Schimmel II', 'vandervort.jermey@example.com', '2019-05-09 11:43:58', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'x0bOOHYLFp', '2019-05-09 11:43:58', '2019-05-09 11:43:58'),
(4, 'Ivah Ankunding', 'stark.leonardo@example.net', '2019-05-09 11:43:58', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'GVIf6jcMot', '2019-05-09 11:43:58', '2019-05-09 11:43:58'),
(5, 'Yessenia Lakin', 'ernestine.eichmann@example.org', '2019-05-09 11:43:58', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'gHwGkdmcys', '2019-05-09 11:43:58', '2019-05-09 11:43:58'),
(6, 'Marquis Rippin', 'elmore.weissnat@example.org', '2019-05-09 11:43:58', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '5iexBcYNdu', '2019-05-09 11:43:58', '2019-05-09 11:43:58'),
(7, 'Mr. Tremaine Bernhard II', 'yprosacco@example.net', '2019-05-09 11:43:58', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'O3kyLxYOHC', '2019-05-09 11:43:58', '2019-05-09 11:43:58'),
(8, 'Prof. Jettie Frami', 'ssenger@example.net', '2019-05-09 11:43:58', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'veKZMdAh9u', '2019-05-09 11:43:58', '2019-05-09 11:43:58'),
(9, 'Rosanna Hills', 'nwintheiser@example.com', '2019-05-09 11:43:58', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'koc1ed5whf', '2019-05-09 11:43:58', '2019-05-09 11:43:58'),
(10, 'Brain Kuhic', 'lila13@example.net', '2019-05-09 11:43:58', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'GvJp8Wu750', '2019-05-09 11:43:58', '2019-05-09 11:43:58'),
(11, 'Cora Hansen', 'ckub@example.net', '2019-05-09 11:43:58', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Dzk2WjLq5H', '2019-05-09 11:43:58', '2019-05-09 11:43:58'),
(12, 'Filomena Borer', 'mafalda00@example.net', '2019-05-09 11:43:58', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'AVltIPc3ks', '2019-05-09 11:43:58', '2019-05-09 11:43:58'),
(13, 'Kelly Torphy', 'chasity90@example.com', '2019-05-09 11:43:58', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'NY8MFL5YJk', '2019-05-09 11:43:58', '2019-05-09 11:43:58'),
(14, 'Lilian Reynolds', 'tyrese83@example.com', '2019-05-09 11:43:58', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '8BZB28OPV4', '2019-05-09 11:43:58', '2019-05-09 11:43:58'),
(15, 'Grace Kling', 'wanderson@example.org', '2019-05-09 11:43:58', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'sELRnCA2kn', '2019-05-09 11:43:58', '2019-05-09 11:43:58'),
(16, 'Tess Schiller', 'brakus.kirstin@example.org', '2019-05-09 11:43:58', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'pz1lcKgWrg', '2019-05-09 11:43:58', '2019-05-09 11:43:58'),
(17, 'Hans Jones', 'schmitt.izaiah@example.net', '2019-05-09 11:43:58', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'bq7Raz5WLK', '2019-05-09 11:43:58', '2019-05-09 11:43:58'),
(18, 'Marcellus Lueilwitz', 'yblanda@example.net', '2019-05-09 11:43:58', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'BRgkkMnbq4', '2019-05-09 11:43:58', '2019-05-09 11:43:58'),
(19, 'Raphaelle Wilderman MD', 'stone96@example.org', '2019-05-09 11:43:58', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'XIhOcq48rt', '2019-05-09 11:43:58', '2019-05-09 11:43:58'),
(20, 'Ophelia Dickens III', 'waelchi.boyd@example.com', '2019-05-09 11:43:58', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'rRjzvHTh78', '2019-05-09 11:43:58', '2019-05-09 11:43:58'),
(21, 'Prof. Geo Kovacek MD', 'alessia.kris@example.org', '2019-05-09 12:24:30', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'G7hbaI5fTD', '2019-05-09 12:24:31', '2019-05-09 12:24:31'),
(22, 'Tyshawn Stoltenberg', 'zdooley@example.com', '2019-05-09 12:24:31', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'dzoSDVZmwg', '2019-05-09 12:24:31', '2019-05-09 12:24:31'),
(23, 'Derek Carroll', 'rodrigo29@example.org', '2019-05-09 12:24:31', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '3gK1LmZvwN', '2019-05-09 12:24:31', '2019-05-09 12:24:31'),
(24, 'Tamara Waters', 'bertram59@example.net', '2019-05-09 12:24:31', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Ji7cg3i6pf', '2019-05-09 12:24:31', '2019-05-09 12:24:31'),
(25, 'Mrs. Annetta West', 'bkunze@example.com', '2019-05-09 12:24:31', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'xvsfIRH39F', '2019-05-09 12:24:31', '2019-05-09 12:24:31'),
(26, 'Micaela Schmidt', 'mortimer53@example.net', '2019-05-09 12:24:31', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'TxZLjJ0HEc', '2019-05-09 12:24:31', '2019-05-09 12:24:31'),
(27, 'Tara Bruen', 'kathryn.cassin@example.org', '2019-05-09 12:24:31', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'xKPoTWRF7H', '2019-05-09 12:24:31', '2019-05-09 12:24:31'),
(28, 'Mrs. Luisa Macejkovic II', 'reinger.coleman@example.com', '2019-05-09 12:24:31', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'xp7kxH4YIq', '2019-05-09 12:24:31', '2019-05-09 12:24:31'),
(29, 'Haylee Prohaska', 'shaina.casper@example.net', '2019-05-09 12:24:31', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'wwcdnTLnFf', '2019-05-09 12:24:31', '2019-05-09 12:24:31'),
(30, 'Miss Kiera Ryan', 'pfritsch@example.net', '2019-05-09 12:24:31', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'E6OSkHTOjQ', '2019-05-09 12:24:31', '2019-05-09 12:24:31'),
(31, 'Delpha Barrows DVM', 'ziemann.anthony@example.com', '2019-05-09 12:24:31', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '8C4VF55ZCp', '2019-05-09 12:24:31', '2019-05-09 12:24:31'),
(32, 'Melvina Bins', 'coty.christiansen@example.com', '2019-05-09 12:24:31', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'XLbeFn9mst', '2019-05-09 12:24:31', '2019-05-09 12:24:31'),
(33, 'Lindsay Sporer MD', 'bode.karolann@example.net', '2019-05-09 12:24:31', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'yvcZTzRhp3', '2019-05-09 12:24:31', '2019-05-09 12:24:31'),
(34, 'Brielle Brakus', 'vhaag@example.net', '2019-05-09 12:24:31', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'MUJcVs24TW', '2019-05-09 12:24:31', '2019-05-09 12:24:31'),
(35, 'Prof. Britney Pacocha', 'franecki.conrad@example.org', '2019-05-09 12:24:31', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '8tLaKLvLbP', '2019-05-09 12:24:31', '2019-05-09 12:24:31'),
(36, 'Fatima Kozey', 'rosemary92@example.com', '2019-05-09 12:24:31', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'fDVU9U1DKZ', '2019-05-09 12:24:31', '2019-05-09 12:24:31'),
(37, 'Maci Bins', 'xrempel@example.org', '2019-05-09 12:24:31', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ygJDrzO4D1', '2019-05-09 12:24:31', '2019-05-09 12:24:31'),
(38, 'Erich Hickle', 'fredy.renner@example.com', '2019-05-09 12:24:31', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '7qVq6gsLe6', '2019-05-09 12:24:31', '2019-05-09 12:24:31'),
(39, 'Manuela Larkin', 'sigmund12@example.net', '2019-05-09 12:24:31', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'nIwmOAYick', '2019-05-09 12:24:31', '2019-05-09 12:24:31'),
(40, 'Greyson Kovacek', 'constantin.crooks@example.org', '2019-05-09 12:24:31', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '6ePkVoHVB3', '2019-05-09 12:24:31', '2019-05-09 12:24:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobiles`
--
ALTER TABLE `mobiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `passports`
--
ALTER TABLE `passports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `mobiles`
--
ALTER TABLE `mobiles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `passports`
--
ALTER TABLE `passports`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
