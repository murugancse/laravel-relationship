<?php

use Illuminate\Database\Seeder;

class MobilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run($user)
    {
        DB::table('mobiles')->insert([
            'mobile' => Str::random(10),
            'user_id' => $user->id,
        ]);
    }
}
