<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
		DB::table('roles')->insert([
            'role_name' => 'Super Admin',
        ]);
		DB::table('roles')->insert([
            'role_name' => 'Admin',
        ]);
		
		factory(App\country::class, 5)->create()->each(function ($country) {
			
			
		});
		
		/* factory(App\state::class, 20)->create()->each(function ($state) {
				
			
		});
		
		factory(App\city::class, 20)->create()->each(function ($city) {
			
		}); */
		
		factory(App\User::class, 20)->create()->each(function ($user) {
			DB::table('passports')->insert([
				'number' => mt_rand(100000000, 999999999),
				'user_id' => $user->id,
			]);
			
			DB::table('mobiles')->insert([
				'mobile' => mt_rand(10000000, 99999999),
				'user_id' => $user->id,
			]);
			
			DB::table('mobiles')->insert([
				'mobile' => mt_rand(10000000, 99999999),
				'user_id' => $user->id,
			]);
			
			DB::table('role_user')->insert([
				'role_id' => 1,
				'user_id' => $user->id,
			]);
			
			DB::table('role_user')->insert([
				'role_id' => 2,
				'user_id' => $user->id,
			]);
			
			//$this->call(PassportsTableSeeder::class,$user);
			//$user->passport()->save(factory(App\passport::class)->make());
			//$user->mobiles()->save(factory(App\mobile::class)->make());
			//$user->mobiles()->save(factory(App\mobile::class)->make());
		});
		
		/* DB::table('users')->insert([
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'password' => bcrypt('secret'),
        ]); */
        // $this->call(UsersTableSeeder::class);
    }
}
