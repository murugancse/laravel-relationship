<?php

use Illuminate\Database\Seeder;

class PassportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run($user)
    {
         DB::table('passports')->insert([
            'number' => Str::random(10),
            'user_id' => $user->id,
        ]);
    }
}
