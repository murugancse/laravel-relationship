<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
	
	/* one to one */
	public function passport(){
		return $this->hasOne(passport::class, 'user_id');
	}
	
	/* one to many */
	public function mobiles(){
		return $this->hasMany(mobile::class, 'user_id');
	}
	
	/* many to many */
	public function roles(){
		return $this->BelongsToMany(role::class);
	}
}
